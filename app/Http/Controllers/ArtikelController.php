<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\Artikel;
use App\Models\Comment;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class ArtikelController extends Controller
{

    public function index()
    {
        return view('artikel', [
            'title' => 'Artikel',
            'artikel' => Artikel::latest()->get(),
            'artikel_1' => Artikel::latest()->first(),
            'kategori' => Kategori::all()
        ]);
    }

    public function detail($id)
    {
        $artikel = Artikel::where('id', $id)->first();
        return view('artikel-detail', [
            'title' => 'Artikel Detail',
            'artikel' => $artikel,
            'user' => User::all(),
            'kategori' => Kategori::all(),
            'komentar' => Comment::all()
        ]);
    }

    public function dataartikel()
    {
        return view('dashboard/artikel/data-artikel', [
            'title' => 'Data artikel',
            'artikel' => Artikel::latest()->get(),
            'user' => User::all(),
            'kategori' => Kategori::all()
        ]);
    }

    public function tambahdataartikel()
    {
        return view('dashboard/artikel/tambah-data-artikel', [
            'title' => 'Tambah Data artikel',
            'kategori' => Kategori::all(),
            'artikel' => Artikel::latest()->get(),
        ]);
    }

    public function store(Request $request)
    {
        $validatedData =  $request->validate([
            'nama' => 'required|max:255',
            'slug' => 'required|unique:artikels',
            'kategori_id' => 'required',
            'deskripsi' => 'required',
            'foto' => 'image|file|max:10240'
        ]);

        $validatedData['user_id'] = auth()->user()->id;

        if ($request->file('foto')) {
            $validatedData['foto'] = $request->file('foto')->store('foto-artikel');
        }

        Artikel::create($validatedData);

        return redirect('/dashboard/data-artikel')->with('success', 'Data berhasil ditambahkan!');
    }

    public function delete($id)
    {
        // $data = Artikel::find($id);
        // $data->delete();

        DB::table('artikels')->where('id', $id)->delete();

        return redirect('/dashboard/data-artikel')->with('success', 'Artikel berhasil dihapus');
    }

    public function edit($id)
    {
        $artikel = DB::table('artikels')->where('id', $id)->first();
        return view('dashboard/artikel/edit', [
            'title' => 'Edit Data artikel',
            'artikel' => $artikel,
            'kategori' => Kategori::all()
        ]);
    }

    public function editdataartikel(Request $request, $id)
    {
        $validatedData =  $request->validate([
            'nama' => 'required|max:255',
            'slug' => 'required',
            'kategori_id' => 'required',
            'deskripsi' => 'required',
            'foto' => 'required|file'
        ]);

        if ($request->file('foto')) {
            $validatedData['foto'] = $request->file('foto')->store('foto-artikel');
        }

        Artikel::where('id', $id)->update($validatedData);
        return redirect('/dashboard/data-artikel')->with('success', 'Artikel berhasil diubah');
    }
}