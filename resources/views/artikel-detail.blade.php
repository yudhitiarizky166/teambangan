@extends('layout.main')

@section('container')

<section id="blog" class="gtn-section gtn-py-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="artikel-header">
            </div>
            <diV class="my-3">
                <h2>{{ $artikel->nama }}</h2>
                <img class="img-fluid rounded-16 my-2" src="{{ asset('storage/'.$artikel->foto) }}" />
                <div class="mb-3">
                    <ul class="text-secondary">
                        @foreach ($user as $usr)
                            @if ($usr->id === $artikel->user_id)
                                <li class="d-inline"><img class="me-2" width="20px" src="https://www.svgrepo.com/show/404906/bust-in-silhouette.svg" />{{ $usr->nama }}</li>
                            @endif
                        @endforeach
                        
                        <li class="d-inline"><img class="mx-2" width="20px" src="https://www.svgrepo.com/show/404912/calendar.svg" />{{ $artikel->created_at->diffForHumans() }}</li>
                    </ul>
                </div>
                <div>
                    {!! $artikel->deskripsi !!}
                </div>
            </diV>
        </div>
        <hr>
        <h5>Tuliskan Komentar Anda</h5>
        <hr>
        <form class="row" method="post" action="/dashboard/tambah-data-komentar">
            <input type="text" name="user_id" id="user_id" value="{{ $artikel->user_id }}" hidden>
            <input type="text" name="artikel_id" id="kategori_id" value="{{ $artikel->id }}" hidden>
            @csrf
            <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="simpleinput">nama</label>
                    <input type="text" id="nama" name="nama" value="{{ old('nama') }}" class="form-control">
                    @error('nama')
                    <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="simpleinput">email</label>
                    <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control">
                    @error('email')
                    <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="comment">comment</label>
                    <textarea  value="{{ old('comment') }}" id="comment" class="form-control" name="comment" rows="10" cols="50"></textarea>
                    @error('comment')
                    <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                    @enderror
                </div>

                <button class="btn btn-primary" type="submit">Tambah Komentar</button>
            </div> 
        </form>
        <hr>
        <div>
            @foreach ($komentar as $komen)
                @if ($komen->artikel_id === $artikel->id)
                    @if ($komen->is_allow === 1)
                        <div class="row my-5 px-3">
                            <div class="col-1">
                                <img class="rounded-16" src="{{ asset('assets/images/default.png') }}" />
                            </div>
                            <div class="col-11 ">
                                <strong>{{ $komen->nama }}</strong>
                                <span class="text-secondary small text-justify">{{ $komen->created_at->diffForHumans() }}</span>
                                <p>{!! $komen->comment !!}</p>
                            </div>
                        </div
                    @endif
                @endif
            @endforeach

        </div>
    </div>

</section>

@endsection