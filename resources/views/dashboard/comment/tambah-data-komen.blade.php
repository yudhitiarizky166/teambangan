@extends('dashboard.layout.main')

@section('container')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h2 class="page-title">Tambah Data komentar</h2>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Form komentar</strong>
                </div>
                <div class="card-body">
                    <form class="row" method="post" action="/dashboard/tambah-data-komentar">
                        <input type="text" name="user_id" id="user_id" value="1" hidden>
                        <input type="text" name="artikel_id" id="kategori_id" value="1" hidden>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group mb-3">
                                <label for="simpleinput">nama</label>
                                <input type="text" id="nama" name="nama" value="{{ old('nama') }}" class="form-control">
                                @error('nama')
                                <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="simpleinput">email</label>
                                <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control">
                                @error('email')
                                <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="comment">comment</label>
                                <textarea  value="{{ old('comment') }}" id="comment" class="form-control" name="comment" rows="10" cols="50"></textarea>
                                @error('comment')
                                <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                                @enderror
                            </div>

                            

                            <button class="btn btn-primary" type="submit">Tambah Data</button>
                        </div> 
                    </form>
                </div> <!-- / .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> 
</div>


@endsection