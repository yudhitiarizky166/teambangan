@extends('layout.main')

@section('container')

<section id="home" class="gtn-section">
  <div class="gtn-section bg-white gtn-hero-section-top-padding">
    <div class="container gtn-pb-100">
      <!--== row starts ==-->
      <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 ">
          <div id="carouselExampleSlidesOnly" class="carousel slide " data-bs-ride="carousel">
            <div class="carousel-inner rounded-16">
              <div class="carousel-item active">
                <img src="https://cms.disway.id/uploads/59d185b6276131ee5f8c40caac0a91f5.jpeg" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="https://mmc.tirto.id/image/otf/1024x535/2017/01/12/antarafoto-aksi-bela-rakyat-120117-riv-2.jpg" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="https://cdn-2.tstatic.net/tribunnews/foto/bank/images/emak-emak-dukung-demo-minyak-11-april.jpg" class="d-block w-100" alt="...">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 my-3">
          <!-- animated hedline starts -->
          <p class="gtn-animated-headline font-weight-medium text-left slide color-dark">
            <span class="gtn-words-wrapper w-100">
              <!--== text starts ==-->
              <!-- first line -->
              <b class="is-visible">Suarakan Hak Kita
                <img draggable="false" role="img" class="emoji" alt="📣" src="https://www.svgrepo.com/show/402138/megaphone.svg" width="22" /></b>
              <!-- second line -->
              <b class="is-hidden">
                Melek Regulasi
                <img draggable="false" role="img" class="emoji" alt="👀" src="https://www.svgrepo.com/show/401468/eyes.svg" width="22" />
              </b>
              <!-- third line -->
              <b class="is-hidden">
                Cerdaskan Bangsa
                <img draggable="false" role="img" class="emoji" alt="🧠" src="https://www.svgrepo.com/show/404878/brain.svg" width="22" />
              </b>
              <!--== text ends ==-->
            </span>
          </p>
          <!-- animated hedline ends -->

          <h1>Proyek Orasi</h1>
          <p class="gtn-intro-content color-dark">
            Unjuk rasa ialah sebagai bentuk dari penyampaian pendapat di muka umum dan hak legal warga negara yang dijamin oleh undang-undang.
            <strong>Proyek Orasi</strong> sebagai wadah bagi aktivis dalam menyampaikan pendapatnya mengenai regulasi yang dibuat pemerintahan.
          </p>
        </div>


      </div>
      <!--== row ends ==-->
    </div>
  </div>
</section>

<section class="gtn-section gtn-py-100 my-1 about">
  <div class="container">
    <!--== row starts ==-->
    <div class="row">
      <!-- column 1 starts -->
      <!-- <div class="col-12 col-md-6">
        <img src="assets/images/favicon.png" alt="image" height="500px" />
      </div> -->
      <!-- column 1 ends -->

      <!-- column 2 starts -->
      <div class="col-12 col-md-12 small-device-space align-items-center d-flex">
        <!-- heading starts -->
        <div class="gtn-section-intro text-left">
          <div class="gtn-intro-subheading-wrapper">
            <p class="gtn-intro-subheading">Berita Terbaru</p>
          </div>
          <h2 class="gtn-intro-heading mb-5">Proyek Orasi</h2>
          <!--== row starts ==-->
          <div class="row">
            <!-- column 3 starts -->
            <div class="col-12 col-md-12 col-sm-12 mb-5">
              <div class=" gtn-post-item">
                <div class="gtn-post-img"><img src="{{ asset('storage/'.$artikel_1->foto) }}" alt="image" /></div>
                <div class="gtn-post-content">
                  @foreach ($kategori as $kat)
                    @if ($kat->id === $artikel_1->kategori_id )
                    <span class="gtn-meta-category">{{ $kat->nama }}</span>
                    @endif
                    
                  @endforeach
                  
                  <h5 class="gtn-post-title mb-0"><a href="/article-details/{{$artikel_1->id}}" rel="bookmark">{{ $artikel_1->nama }}</a></h5>
                  <div class="hide">
                    {{-- <p class="gtn-post-excerpt"> {!! $artikel_1->deskripsi !!}</p> --}}
                    <a class="gtn-read-more  mt-1" href="/article-details/{{$artikel_1->id}}"><span class="gtn-read-more-content">Selengkapnya ...</span></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- column 3 ends -->

            @foreach ($artikel as $art)
            <div class="col-6 col-md-4 col-sm-6 mb-5">
              <div class="gtn-post-item">
                <div class="gtn-post-img"><img src="{{ asset('storage/'.$art->foto) }}" alt="image" /></div>
                <div class="gtn-post-content">
                  @foreach ($kategori as $kat)
                    @if ($kat->id === $art->kategori_id )
                    <span class="gtn-meta-category">{{ $kat->nama }}</span>
                    @endif
                  @endforeach
                  <h5 class="gtn-post-title mb-1"><a href="/article-details/{{$art->id}}" rel="bookmark">{{ $art->nama }}</a></h5>
                  <div class="hide">
                    {{-- <p class="gtn-post-excerpt">{!! $art->deskripsi !!}</p> --}}
                    <a class="gtn-read-more mt-0" href="/article-details/{{$art->id}}"><span class="gtn-read-more-content">Selengkapnya ...</span></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            <!-- column 3 starts -->
            <!-- column 3 ends -->

            <!-- column 3 ends -->
            <!-- <p class="gtn-intro-content">
            GeoTani solusi hebat untuk petani cermat. Geotani merupakan
            website yang menyediakan berbagai layanan seperti
            rekomendasi tanaman, artikel, dan juga video edukasi.
          </p> -->
            <!-- button -->
            <!-- <a class="gtn-btn btn-blue gtn-px-lg gtn-mt-50" href="#" role="button"> <span class="gtn-btn-text">Tonton
            Sekarang</span> </a> -->
          </div>
          <!-- heading ends -->
        </div>
        <!-- column 2 ends -->
      </div>
      <!--== row ends ==-->
    </div>
</section>

<!-- hero section ends
================================================== -->
<!-- <section>
  <div class="gtn-section-intro text-center gtn-mb-50 mt-5">
    <div class="gtn-intro-subheading-wrapper">
      <p class="gtn-intro-subheading">Geotani</p>
    </div>
    <h2 class="gtn-intro-heading">Rekomendasi Tanaman</h2>
    <h3 class="gtn-intro-heading">Untukmu</h3>
    <p class="gtn-intro-content">
      Cari lokasimu & lihat tanaman apa yang bisa ditanam didaerahmu
    </p>
  </div>
</section> -->


@endsection