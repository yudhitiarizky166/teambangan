<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\KategoriController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/artikel', [ArtikelController::class, 'index']);
Route::get('/tentang-kami', [HomeController::class, 'tentangkami']);
Route::get('/artikel-details', [ArtikelController::class, 'details']);
Route::get('/article-details/{artikel:id}', [ArtikelController::class, 'detail']);

Route::get('/login', [HomeController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login', [HomeController::class, 'authenticate']);
Route::get('/signup', [HomeController::class, 'register'])->middleware('guest');
Route::post('/signup', [HomeController::class, 'store']);
Route::post('/logout', [HomeController::class, 'logout']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [HomeController::class, 'dashboard']);

    Route::get('/dashboard/data-artikel', [ArtikelController::class, 'dataartikel']);
    Route::get('/dashboard/tambah-data-artikel', [ArtikelController::class, 'tambahdataartikel']);
    Route::post('/dashboard/tambah-data-artikel', [ArtikelController::class, 'store']);
    Route::patch('/dashboard/data-artikel/{id}', [ArtikelController::class, 'editdataartikel']);
    Route::get('/dashboard/data-artikel/edit/{id}', [ArtikelController::class, 'edit']);
    Route::delete('/dashboard/data-artikel/{artikel:id}', [ArtikelController::class, 'delete']);

    Route::get('/dashboard/data-kategori', [KategoriController::class, 'datakategori']);
    Route::get('/dashboard/tambah-data-kategori', [KategoriController::class, 'tambahdatakategori']);
    Route::post('/dashboard/tambah-data-kategori', [KategoriController::class, 'store']);
    Route::get('/dashboard/data-kategori/edit/{id}', [KategoriController::class, 'edit']);
    Route::put('/dashboard/data-kategori/{id}', [KategoriController::class, 'editdatakategori']);
    Route::delete('/dashboard/data-kategori/{kategori:id}', [KategoriController::class, 'delete']);

    Route::get('/dashboard/komentar', [CommentController::class, 'datakomentar']);
    Route::post('/dashboard/data-Komen/{id}', [CommentController::class, 'allow']);
    Route::patch('/dashboard/data-Komen/{id}', [CommentController::class, 'not_allow']);
    Route::delete('/dashboard/data-Komen/{id}', [CommentController::class, 'delete']);
    Route::post('/dashboard/tambah-data-komentar', [CommentController::class, 'store']);
    Route::get('/dashboard/tambah-data-komentar', [CommentController::class, 'tambahdatakomentar']);
});